// В первой строке входных данных содержится целое число t (1≤t≤104) — количество наборов входных данных в тесте.

// Далее следуют описания t наборов входных данных, один набор в строке.

// В первой (и единственной) строке набора записаны два целых числа a и b (−1000≤a,b≤1000).

package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {

	in := bufio.NewReader(os.Stdin)
	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()
	var t, a, b int //  t- наборы; a, b - целые числа

	fmt.Fscan(in, &t)

	for i := 0; i < t; i++ {
		fmt.Fscan(in, &a, &b)
		fmt.Fprintln(out, a+b)
	}

}
